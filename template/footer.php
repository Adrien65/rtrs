<!-- footer.php -->
<footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Restaurant Table Reservation System</h2>
              <p>Les meilleurs tables de l'agglomération Tarbes-Lourdes-Pyrénées</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Horaires</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Lundi: <span>08:00 - 22:00</span></a></li>
                <li><a href="#" class="py-2 d-block">Mardi: <span>08:00 - 22:00</span></a></li>
                <li><a href="#" class="py-2 d-block">Mercredi: <span>08:00 - 22:00</span></a></li>
                <li><a href="#" class="py-2 d-block">Jeudi: <span>08:00 - 22:00</span></a></li>
                <li><a href="#" class="py-2 d-block">Vendredi: <span>08:00 - 22:00</span></a></li>
                <li><a href="#" class="py-2 d-block">Samedi: <span>08:00 - 22:00</span></a></li>
                <li><a href="#" class="py-2 d-block">Dimanche: <span>08:00 - 22:00</span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Contact</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">lmct65</a></li>
                <li><a href="#" class="py-2 d-block">0123456789</a></li>
                <li><a href="#" class="py-2 d-block">lmct65@local.fr</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Newsletter</h2>
              <p>Pour rester informé des nouveautés</p>
              <form action="#" class="subscribe-form">
                <div class="form-group">
                  <span class="icon icon-paper-plane"></span>
                  <input type="text" class="form-control" placeholder="Subscribe">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <p>
              Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved<i class="icon-heart"aria-hidden="true"></i> by <a href="https://projectworlds.in" target="_blank">Projectworlds</a>
            </p>
          </div>
        </div>
      </div>
    </footer>

    <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
